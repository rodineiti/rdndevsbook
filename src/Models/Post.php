<?php

namespace Src\Models;

use Src\Core\Model;

class Post extends Model
{
    public function __construct()
    {
        parent::__construct("posts");
    }

    public function all($where = [], $whereIn = [], $limit = null, $offset = null)
    {
        $this->order = "posts.created_at DESC";
        $results = $this->read(true, ["*"], $where, $whereIn, $limit, $offset) ?? [];

        foreach ($results as $result) {
            $result->user = (new User())->getById($result->user_id);
            $result->comments = (new PostComment())->getByPost($result->id);
            $result->likes = (new PostLike())->getCountByPost($result->id);
            $result->owner = ($result->user_id === auth()->id ? true : false);
            $result->ownerLiked = (new PostLike())->getCountByPostAndUser($result->id, auth()->id);
            if ($result->type === "photo") {
                $result->body = media("uploads/".$result->body);
            }
        }

        return $results;
    }

    public function create(array $data)
    {
        $data["user_id"] = auth()->id;

        if (isset($data["photoFile"]) && count($data["photoFile"])) {
            if (in_array($data["photoFile"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["body"] = cutImage(
                    $data["photoFile"],
                    800,
                    800,
                    "media/uploads"
                );
            }
        }

        unset($data["photoFile"]);

        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }
        return null;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            $model->owner = ($model->user_id === auth()->id ? true : false);
            return $model;
        }
        return null;
    }

    public function destroy($post)
    {
        /**
         * Deleta
         * comentários
         * likes
         */
        (new PostLike())->delete(["post_id" => $post->id]);
        (new PostComment())->delete(["post_id" => $post->id]);

        if ($post->type === "photo") {
            $this->removeFile($post->body);
        }

        if ($this->delete(["id" => $post->id])) {
            return true;
        }
        return false;
    }

    private function removeFile($file)
    {
        if (!empty($file))
        {
            $filePath = CONF_UPLOAD_FILE_UPLOADS."/".$file;

            if (file_exists($filePath)) 
            {
                @unlink($filePath);
            }
        }
    }
}

?>