<?=$this->view("alerts")?>
<form method="POST" action="<?= BASE_URL ?>auth/save">
    <input placeholder="Digite seu nome" class="input" type="text" name="name" required />
    <input placeholder="Digite seu e-mail" class="input" type="email" name="email" required />
    <input placeholder="Digite sua senha" class="input" type="password" name="password" required />
    <input placeholder="00/00/0000" class="input" type="date" name="birthdate" required />
    <input class="button" type="submit" value="Criar conta" />
    <a href="<?= BASE_URL?>auth?login">Já tenho conta? Acessar</a>
</form>
