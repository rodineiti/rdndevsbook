<?=$this->view("aside")?>

<section class="feed mt-10">

    <div class="row">
        <div class="column pr-5">

            <?=$this->view("alerts")?>
            <br>

            <?=$this->view("feed_new", ["user" => auth()])?>

            <?php foreach ($posts as $post): ?>
                <?=$this->view("post_item", ["post" => $post])?>
            <?php endforeach; ?>

            <?=$this->view("pagination", ["pages" => $pages, "page" => $page, "linkPage" => "home/index"])?>

        </div>

        <?=$this->view("sponsor")?>
    </div>

</section>