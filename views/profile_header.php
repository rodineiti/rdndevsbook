<div class="row">
    <div class="box flex-1 border-top-flat">
        <div class="box-body">
            <?php if ($user->cover): ?>
                <div class="profile-cover" style="background-image: url('<?=media("covers/".$user->cover)?>');"></div>
            <?php else: ?>
                <div class="profile-cover" style="background-image: url('<?=media("covers/cover.jpg")?>');"></div>
            <?php endif; ?>
            <div class="profile-info m-20 row">
                <div class="profile-info-avatar">
                    <a href="<?=BASE_URL."profile/show/".$user->id?>">
                    <?php if ($user->avatar): ?>
                        <img src="<?=media("avatars/".$user->avatar)?>" alt="avatar" />
                    <?php else: ?>
                        <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                    <?php endif; ?>
                    </a>
                </div>
                <div class="profile-info-name">
                    <div class="profile-info-name-text">
                        <a href="<?=BASE_URL."profile/show/".$user->id?>">
                            <?=$user->name?>
                        </a>
                    </div>
                    <?php if (!empty($user->city)): ?>
                        <div class="profile-info-location"><?=$user->city?></div>
                    <?php endif; ?>
                </div>
                <div class="profile-info-data row">
                    <?php if ($user->id !== auth()->id): ?>
                        <div class="profile-info-item m-width-20">
                            <a href="<?=BASE_URL . "profile/follow/".$user->id?>" class="button">
                                <?=($isFollowing) ? "- Deixar de seguir" : "+ Seguir"?>
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="profile-info-item m-width-20">
                        <a href="<?=BASE_URL."friends/show/".$user->id?>">
                            <div class="profile-info-item-n"><?=count($user->followers)?></div>
                            <div class="profile-info-item-s">Seguidores</div>
                        </a>
                    </div>
                    <div class="profile-info-item m-width-20">
                        <a href="<?=BASE_URL."friends/show/".$user->id?>">
                            <div class="profile-info-item-n"><?=count($user->followings)?></div>
                            <div class="profile-info-item-s">Seguindo</div>
                        </a>
                    </div>
                    <div class="profile-info-item m-width-20">
                        <a href="<?=BASE_URL."photos/show/".$user->id?>">
                            <div class="profile-info-item-n"><?=count($user->photos)?></div>
                            <div class="profile-info-item-s">Fotos</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>