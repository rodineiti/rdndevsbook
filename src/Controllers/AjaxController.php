<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Post;
use Src\Models\PostComment;
use Src\Models\PostLike;

class AjaxController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->auth();
    }

    public function like($post_id)
    {
        if ($this->method() !== "GET") {
            $this->json(["error" => true, "message" => "Method not allowed"]);
        }

        $post = (new Post())->getById($post_id);

        if (!$post) {
            $this->json(["error" => true, "message" => "Post not found"]);
        }

        (new PostLike())->like($post->id, auth()->id);
        $this->json(["error"=> false, "message" => "Like success"]);
    }

    public function comment()
    {
        if ($this->method() !== "POST") {
            $this->json(["error" => true, "message" => "Method not allowed"]);
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($request["id"])) {
            $this->json(["error" => "Id is required"]);
        }

        if (empty($request["body"])) {
            $this->json(["error" => "Body this comment is required"]);
        }

        $post = (new Post())->getById($request["id"]);

        if (!$post) {
            $this->json(["error" => true, "message" => "Post not found"]);
        }

        $comment = (new PostComment())->create([
            "post_id" => $post->id,
            "body" => $request["body"]
        ]);

        if ($comment) {
            $this->json(["error"=> false, "message" => "Comment success", "data" => $comment]);
        }

        $this->json(["error" => true, "message" => "Not posted comment"]);
    }

    public function upload()
    {
        if ($this->method() !== "POST") {
            $this->json(["error" => true, "message" => "Method not allowed"]);
        }

        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $data["photoFile"] = isset($_FILES['photo']) && !empty($_FILES["photo"]["tmp_name"]) ? $_FILES['photo'] : [];

        if (count($data["photoFile"])) {
            $data["type"] = "photo";
            if (!(new Post())->create($data)) {
                $this->json(["error"=> true, "message" => "Não foi possível adicionar a foto, tente novamente."]);
            }
            setFlashMessage("success", ["Foto enviada com sucesso."]);
            $this->json(["error"=> false, "message" => "Foto enviada com sucesso."]);
        }
        $this->json(["error"=> true, "message" => "Não foi possível adicionar a foto, tente novamente."]);
    }
}