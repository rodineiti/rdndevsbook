<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Post;

class PostsController extends Controller
{
    protected $data;
    protected $required;
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
        $this->required = ["body"];
        $this->model = new Post();
    }

    public function store()
    {
        $body = $this->request()["body"];
        $data = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $data["body"] = trim($body);

        if (!$this->required($data)) {
            setFlashMessage("danger", ["Favor informar o texto para publicar seu post."]);
            $this->back();
        }

        $data["type"] = "text";
        if (!$this->model->create($data)) {
            setFlashMessage("danger", ["Não foi possível adicionar, tente novamente."]);
            $this->back();
        }

        setFlashMessage("success", ["Post adicionado com sucesso."]);
        $this->back();
    }

    public function destroy($post_id)
    {
        $post = (new Post())->getById($post_id);

        if (!$post) {
            setFlashMessage("danger", ["Post não encontrado"]);
            $this->back();
        }

        if ($post->user_id !== auth()->id) {
            setFlashMessage("danger", ["Este post não pertence a você"]);
            $this->back();
        }

        $delete = (new Post())->destroy($post);

        if ($delete) {
            setFlashMessage("success", ["Post deletado com sucesso"]);
        }

        setFlashMessage("danger", ["Erro ao deletar o post, tente mais tarde"]);
        $this->back();
    }
}