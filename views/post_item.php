<div class="box feed-item" data-id="<?=$post->id?>">
    <div class="box-body">
        <div class="feed-item-head row mt-20 m-width-20">
            <div class="feed-item-head-photo">
                <a href="<?=BASE_URL."profile/show/".$post->user->id?>">
                    <?php if ($post->user->avatar): ?>
                        <img src="<?=media("avatars/" . $post->user->avatar)?>" alt="avatar" />
                    <?php else: ?>
                        <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                    <?php endif; ?>
                </a>
            </div>
            <div class="feed-item-head-info">
                <a href="<?=BASE_URL."profile/show/".$post->user->id?>"><span class="fidi-name"><?=$post->user->name?></span></a>
                <span class="fidi-action">
                    <?php if ($post->type === "text"): ?>
                        fez um post
                    <?php endif; ?>
                    <?php if ($post->type === "photo"): ?>
                        postou uma foto
                    <?php endif; ?>
                </span>
                <br/>
                <span class="fidi-date"><?=date("d/m/Y H:i", strtotime($post->created_at))?></span>
            </div>
            <?php if ($post->owner): ?>
            <div class="feed-item-head-btn">
                <img src="<?=image("more.png")?>" alt="more" />
                <div class="feed-item-more-window">
                    <a href="<?=BASE_URL."posts/destroy/".$post->id?>">Deletar</a>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="feed-item-body mt-10 m-width-20">
            <?php if ($post->type === "text"): ?>
                <?=nl2br($post->body)?>
            <?php endif; ?>
            <?php if ($post->type === "photo"): ?>
                <img src="<?=$post->body?>" alt="<?=$post->body?>" />
            <?php endif; ?>
        </div>
        <div class="feed-item-buttons row mt-20 m-width-20">
            <div class="like-btn <?=$post->ownerLiked ? 'on' :  ''?>">
                <?=$post->likes?>
            </div>
            <div class="msg-btn"><?=count($post->comments)?></div>
        </div>
        <div class="feed-item-comments">

            <div class="fic-item-area">
                <?php foreach ($post->comments as $comment): ?>
                    <div class="fic-item row m-height-10 m-width-20">
                        <div class="fic-item-photo">
                            <a href="<?=BASE_URL."profile/show/".$comment->user_id?>">
                                <?php if ($comment->avatar): ?>
                                    <img src="<?=media("avatars/".$comment->avatar)?>" alt="avatar" />
                                <?php else: ?>
                                    <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                                <?php endif; ?>
                            </a>
                        </div>
                        <div class="fic-item-info">
                            <a href="<?=BASE_URL."profile/show/".$comment->user_id?>"><?=$comment->name?></a>
                            <?=$comment->body?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="fic-answer row m-height-10 m-width-20">
                <div class="fic-item-photo">
                    <a href="<?=BASE_URL."profile/show/".auth()->id?>">
                    <?php if (auth()->avatar): ?>
                        <img src="<?=media("avatars/" . auth()->avatar)?>" alt="avatar" />
                    <?php else: ?>
                        <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                    <?php endif; ?>
                    </a>
                </div>
                <input type="text" class="fic-item-field" placeholder="Escreva um comentário" />
            </div>

        </div>
    </div>
</div>