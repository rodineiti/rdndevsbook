<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;

class SearchController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
    }

    public function index()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if (empty($request["term"])) {
            $this->redirect();
        }

        $users = (new User())->all(["searchTerm" => $request["term"]]);
        $this->data["users"] = $users;
        $this->data["term"] = $request["term"];
        $this->template("search", $this->data);
    }
}