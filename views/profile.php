<?=$this->view("aside")?>

<section class="feed">

    <?=$this->view("profile_header", ["user" => $user, "isFollowing" => isset($isFollowing) ? $isFollowing : null])?>

    <div class="row">

        <div class="column side pr-5">

            <div class="box">
                <div class="box-body">
                    <?php if (!empty($user->birthdate)): ?>
                    <div class="user-info-mini">
                        <img src="<?=image("calendar.png")?>" alt="calendar" />
                        <?=date("d/m/Y", strtotime($user->birthdate))?> (<?=ageYears($user->birthdate)?> anos)
                    </div>
                    <?php endif; ?>
                    <?php if (!empty($user->city)): ?>
                    <div class="user-info-mini">
                        <img src="<?=image("pin.png")?>" alt="pin" />
                        <?=$user->city?>
                    </div>
                    <?php endif; ?>
                    <?php if (!empty($user->work)): ?>
                    <div class="user-info-mini">
                        <img src="<?=image("work.png")?>" alt="work" />
                        <?=$user->work?>
                    </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="box">
                <div class="box-header m-10">
                    <div class="box-header-text">
                        Seguindo
                        <span>(<?=count($user->followings)?>)</span>
                    </div>
                    <div class="box-header-buttons">
                        <?php if (count($user->followings)): ?>
                            <a href="<?=BASE_URL."friends/show/".$user->id?>">ver todos</a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="box-body friend-list">

                    <?php foreach ($user->followings as $following):?>
                    <div class="friend-icon">
                        <a href="<?=BASE_URL . "profile/show/".$following->id?>">
                            <div class="friend-icon-avatar">
                                <?php if ($following->avatar): ?>
                                    <img src="<?=media("avatars/".$following->avatar)?>" alt="avatar" />
                                <?php else: ?>
                                    <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                                <?php endif; ?>
                            </div>
                            <div class="friend-icon-name">
                                <?=$following->name?>
                            </div>
                        </a>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>

        </div>
        <div class="column pl-5">

            <div class="box">
                <div class="box-header m-10">
                    <div class="box-header-text">
                        Fotos
                        <span>(<?=count($user->photos)?>)</span>
                    </div>
                    <div class="box-header-buttons">
                        <?php if (count($user->photos)): ?>
                            <a href="<?=BASE_URL."photos/show/".$user->id?>">ver todos</a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="box-body row m-20">
                    <?php foreach ($user->photos as $photo): ?>
                    <div class="user-photo-item">
                        <a href="#modal-<?=$photo->id?>" data-modal-open>
                            <img src="<?=$photo->body?>" alt="<?=$photo->body?>" />
                        </a>
                        <div id="modal-<?=$photo->id?>" style="display:none">
                            <img src="<?=$photo->body?>" alt="<?=$photo->body?>" />
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>

            <?=$this->view("alerts")?>
            <br>

            <?php if ($user->id === auth()->id): ?>
                <?=$this->view("feed_new", ["user" => auth()])?>
            <?php endif; ?>

            <?php foreach ($user->posts as $post): ?>
                <?=$this->view("post_item", ["post" => $post])?>
            <?php endforeach; ?>
            <?=$this->view("pagination", ["pages" => $pages, "page" => $page, "linkPage" => $linkPage])?>

        </div>

    </div>

</section>
<script>
    window.onload = function() {
        var modal = new VanillaModal.default();
    }
</script>