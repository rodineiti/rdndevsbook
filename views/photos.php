<?=$this->view("aside")?>

<section class="feed">

    <?=$this->view("profile_header", ["user" => $user])?>

    <div class="row">

        <div class="column">

            <div class="box">
                <div class="box-body">

                    <div class="full-user-photos">

                        <?=$this->view("alerts")?>

                        <?php foreach ($user->photos as $photo): ?>
                            <div class="user-photo-item">
                                <a href="#modal-<?=$photo->id?>" data-modal-open>
                                    <img src="<?=$photo->body?>" alt="<?=$photo->body?>" />
                                </a>
                                <div id="modal-<?=$photo->id?>" style="display:none">
                                    <img src="<?=$photo->body?>" alt="<?=$photo->body?>" />
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>


                </div>
            </div>

        </div>

    </div>

</section>
<script>
    window.onload = function() {
        var modal = new VanillaModal.default();
    }
</script>