<?php

namespace Src\Models;

use Src\Core\Model;

class UserFollow extends Model
{
    public function __construct()
    {
        parent::__construct("users_following");
    }

    public function checkFollow($user_id_from, $user_id_to)
    {
        return $this->count(["id"], ["user_id_from" => $user_id_from, "user_id_to" => $user_id_to]);
    }

    // seguindo
    public function getFollowing($user_id, $me = true)
    {
        $followings = $this->read(true, ["user_id_to"], ["user_id_from" => $user_id]) ?? [];
        $ids = [];

        foreach ($followings as $following) {
            $ids[] = $following->user_id_to;
        }

        if ($me) {
            // add my id
            $ids[] = auth()->id;
        }

        return $ids;
    }

    // seguidores
    public function getFollower($user_id, $me = true)
    {
        $followers = $this->read(true, ["user_id_from"], ["user_id_to" => $user_id]) ?? [];
        $ids = [];

        foreach ($followers as $follower) {
            $ids[] = $follower->user_id_from;
        }

        if ($me) {
            // add my id
            $ids[] = auth()->id;
        }

        return $ids;
    }
}

?>