<div class="feed-pagination">
    <?php if ($pages > 1): ?>
        <?php for($i = 1; $i <= $pages; $i++): ?>
            <a class="page-link <?=($page === $i) ? 'active' : ''?>" href="<?= BASE_URL . $linkPage ?>?<?php
            $pageArray["page"] = $i;
            echo http_build_query($pageArray);
            ?>"><?=$i?></a>
        <?php endfor; ?>
    <?php endif; ?>
</div>