<?php

namespace Src\Models;

use Src\Core\Model;

class PostComment extends Model
{
    public function __construct()
    {
        parent::__construct("posts_comments");
    }

    public function getByPost($post_id)
    {
        $results = $this->readJoin(
            true,
            ["posts_comments.*", "users.name", "users.avatar"],
            ["posts_comments.post_id" => $post_id], ["INNER JOIN users", "posts_comments.user_id = users.id"]) ?? [];

        return $results;
    }

    public function getById($id, $columns = ["*"])
    {
        $model = $this->findById($id, $columns);

        if ($model) {
            $user = (new User())->getById($model->user_id);
            $user->avatar = media("avatars/".$user->avatar);
            $model->user = $user;
            $model->url = "profile/show/".$model->user_id;
            return $model;
        }
        return null;
    }

    public function create(array $data)
    {
        $data["user_id"] = auth()->id;
        $modelId = $this->insert($data);

        if ($modelId) {
            return $this->getById($modelId);
        }
        return null;
    }
}

?>