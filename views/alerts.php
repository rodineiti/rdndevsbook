<?php if ($errors = flashMessage("errors")): ?>
    <div class="notification <?=$errors["status"]?>">
        <?php if (is_array($errors["messages"])): ?>
            <?php foreach ($errors["messages"] as $error): ?>
                <p><?=$error?></p>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_string($errors["messages"])): ?>
            <p><?=$errors["messages"]?></p>
        <?php endif; ?>
    </div>
<?php endif; ?>