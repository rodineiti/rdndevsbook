<div class="box feed-new">
    <div class="box-body">
        <div class="feed-new-editor m-10 row">
            <div class="feed-new-avatar">
                <?php if (isset($user) && $user->avatar): ?>
                    <img src="<?=media("avatars/".$user->avatar)?>" alt="avatar" />
                <?php else: ?>
                    <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                <?php endif; ?>
            </div>
            <div class="feed-new-input-placeholder">O que você está pensando, <?=$user->name?>?</div>
            <div class="feed-new-input" contenteditable="true"></div>
            <div class="feed-new-photo">
                <img src="<?=image("photo.png")?>" alt="send" />
                <input type="file" name="file" class="feed-new-file" accept="image/png,image/jpeg,image/jpg" />
            </div>
            <div class="feed-new-send">
                <img src="<?=image("send.png")?>" alt="send" />
            </div>
            <form class="feed-form-store" action="<?=BASE_URL."posts/store"?>" method="post">
                <input type="hidden" name="body" />
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    let feedInput = document.querySelector(".feed-new-input");
    let feedSubmit = document.querySelector(".feed-new-send");
    let feedForm = document.querySelector(".feed-form-store");
    let feedPhoto = document.querySelector(".feed-new-photo");
    let feedFile = document.querySelector(".feed-new-file");
    let urlUpload = '<?=BASE_URL."ajax/upload"?>';

    feedPhoto.addEventListener("click", function (obj) {
        feedFile.click();
    });

    feedFile.addEventListener("change", async function(e) {
        let photo = feedFile.files[0];
        let formData = new FormData();
        formData.append("photo", photo);
        let request = await fetch(urlUpload, {
            method: "POST",
            body: formData,
        });
        let json = await request.json();

        if (!json.error) {
            window.location.href = window.location.href;
        } else {
            alert('Ocorreu um erro ao postar a foto');
        }
    });

    feedSubmit.addEventListener("click", function (obj) {
        let value = feedInput.innerText.trim();
        if (value !== "") {
            feedForm.querySelector("input[name=body]").value = value;
            feedForm.submit();
        }
    });
</script>