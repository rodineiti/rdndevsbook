<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Post;
use Src\Models\User;
use Src\Models\UserFollow;

class ProfileController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
    }

    public function index()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $limit = $this->limit;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $user = (new User())->getById(auth()->id, ["*"], true, $limit, $offset);

        if (!$user) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            $this->redirect();
        }

        $postsCount = (new Post())->count(["id"], ["user_id" => $user->id, "type" => "text"]);
        $pages = ceil($postsCount / $limit);

        $this->data["user"] = $user;
        $this->data["total"] = count($user->posts);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;
        $this->data["linkPage"] = "profile";
        $this->template("profile", $this->data);
    }

    public function show($user_id)
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $limit = $this->limit;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $user = (new User())->getById($user_id, ["*"], true, $limit, $offset);

        if (!$user) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            $this->redirect();
        }

        $postsCount = (new Post())->count(["id"], ["user_id" => $user->id, "type" => "text"]);
        $pages = ceil($postsCount / $limit);

        $this->data["user"] = $user;
        $this->data["total"] = count($user->posts);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;
        $this->data["linkPage"] = "profile/show/".$user->id;
        $this->data["isFollowing"] = (new UserFollow())->checkFollow(auth()->id, $user->id);
        $this->template("profile", $this->data);
    }

    public function follow($user_id_to)
    {
        $user = (new User())->getById($user_id_to, ["*"]);

        if (!$user) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            $this->back();
        }

        (new User())->follow(auth()->id, $user->id);

        setFlashMessage("success", ["Operação realizada com sucesso."]);
        $this->back();
    }
}