<?=$this->view("aside")?>

<section class="feed">

    <div class="row mt-10">
        <div class="column pr-5">

            <h1>Configurações</h1>

            <?=$this->view("alerts")?>
            <form class="form-profile" method="POST" action="<?= BASE_URL ?>auth/update" enctype="multipart/form-data">
                <label for="avatar">Novo avatar</label>
                <input class="input" id="avatar" type="file" name="avatar" />
                <label for="cover">Nova capa</label>
                <input class="input" id="cover" type="file" name="cover" />
                <br><hr><br>
                <label for="name">Nome</label>
                <input placeholder="Digite seu nome" class="input" id="name" type="text" name="name" value="<?=auth()->name?>" required />
                <label for="email">E-mail</label>
                <input placeholder="Digite seu e-mail" id="email" class="input" type="email" name="email" value="<?=auth()->email?>" required />
                <label for="birthdate">Data Nascimento</label>
                <input placeholder="00/00/0000" class="input" id="birthdate" type="date" name="birthdate" value="<?=auth()->birthdate?>" required />
                <label for="city">Cidade</label>
                <input placeholder="Digite sua cidade" class="input" id="city" type="text" name="city" value="<?=auth()->city?>" required />
                <label for="work">Trabalho</label>
                <input placeholder="Digite seu trabalho" class="input" id="work" type="text" name="work" value="<?=auth()->work?>" required />
                <br><hr><br>
                <label for="password">Nova senha</label>
                <input placeholder="Digite sua senha" id="password" class="input" type="password" name="password" />
                <input class="button" type="submit" value="Atualizar" />
            </form>

        </div>

    </div>

</section>