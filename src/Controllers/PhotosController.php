<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;
use Src\Models\UserFollow;

class PhotosController extends Controller
{
    protected $data;
    protected $required;
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
        $this->model = new User();
    }

    public function index()
    {
        $user = $this->model->getById(auth()->id, ["*"], true);

        if (!$user) {
            setFlashMessage("danger", ["Usuário não encontrado"]);
            $this->redirect();
        }

        if (!count($user->photos)) {
            setFlashMessage("danger", ["Este usuário não possui fotos"]);
        }

        $this->data["user"] = $user;
        $this->template("photos", $this->data);
    }

    public function show($user_id)
    {
        $user = $this->model->getById($user_id, ["*"], true);

        if (!$user) {
            setFlashMessage("info", ["Usuário não encontrado"]);
            $this->redirect();
        }

        if (!count($user->photos)) {
            setFlashMessage("info", ["Este usuário não possui fotos"]);
        }

        $this->data["user"] = $user;
        $this->data["isFollowing"] = (new UserFollow())->checkFollow(auth()->id, $user->id);
        $this->template("photos", $this->data);
    }
}