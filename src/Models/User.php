<?php

namespace Src\Models;

use Src\Core\Model;
use Src\Support\Session;

class User extends Model
{
    public function __construct()
    {
        parent::__construct("users");
    }

    public function all($filters = [], $limit = null, $offset = null)
    {
        $query = "SELECT * FROM users ";

        $where = $this->buildWhere($filters);
        $query .= " WHERE " . implode(" AND ", $where);

        if ($limit) {
            $query .= " LIMIT {$limit} ";
        }

        if ($offset) {
            $query .= " OFFSET {$offset} ";
        }

        $results = $this->customQuery($query) ?? [];

        return $results;
    }

    public function attempt($email, $password)
    {
        $user = $this->read(false, ["*"], ["email" => $email]);

        if (!$user) {
            return false;
        }

        if (!pwd_verify($password, $user->password)) {
            return false;
        }

        if (pwd_rehash($user->password)) {
            $this->update(["password" => pwd_gen_hash($password)],  ["id" => $user->id]);
        }

        return $user;
    }

    public function setSession($user)
    {
        Session::set("userLogged", (object)$user);
    }

    public function destroySession()
    {
        Session::destroy("userLogged");
    }

    public function create(array $data)
    {
        if (!filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        if ($this->exists("email", $data["email"])) {
            return false;
        }

        $data["password"] = pwd_gen_hash($data["password"]);

        $userId = $this->insert($data);

        if ($userId) {
            return $this->getById($userId);
        }

        return null;
    }

    public function getById($id, $columns = ["*"], $full = false, $limit = null, $offset = null)
    {
        $user = $this->findById($id, $columns);

        if ($user) {
            if ($full) {
                $user->posts = (new Post())->all(["user_id" => $user->id, "type" => "text"], [], $limit, $offset);
                $user->photos = (new Post())->all(["user_id" => $user->id, "type" => "photo"], [], $limit);
                $user->followings = $this->getFollowings($user->id);
                $user->followers = $this->getFollowers($user->id);
            }
            return $user;
        }
        return null;
    }

    public function updateProfile($user, array $data)
    {
        if (!empty($data["password"])) {
            $data["password"] = pwd_gen_hash($data["password"]);
        } else {
            unset($data["password"]);
        }

        if (isset($data["email"]) && !filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            setFlashMessage("danger", ["E-mail informado é inválido."]);
            return false;
        }

        if (isset($data["email"]) && $this->exists("email", $data["email"], $user->id)) {
            setFlashMessage("danger", ["Já existe um usuário com este e-mail."]);
            return false;
        }

        if (isset($data["avatarFile"]) && count($data["avatarFile"])) {
            if (in_array($data["avatarFile"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["avatar"] = cutImage(
                    $data["avatarFile"],
                    CONF_WIDTH_AVATAR,
                    CONF_HEIGHT_AVATAR,
                    "media/avatars"
                );
                $this->removeFile(CONF_UPLOAD_FILE_AVATARS, $user->avatar);
            }
        }

        if (isset($data["coverFile"]) && count($data["coverFile"])) {
            if (in_array($data["coverFile"]["type"], ["image/jpeg", "image/jpg", "image/png"])) {
                $data["cover"] = cutImage(
                    $data["coverFile"],
                    CONF_WIDTH_COVER,
                    CONF_HEIGHT_COVER,
                    "media/covers"
                );
                $this->removeFile(CONF_UPLOAD_FILE_COVERS, $user->cover);
            }
        }

        unset($data["email"]);
        unset($data["avatarFile"]);
        unset($data["coverFile"]);

        if (count($data)) {
            if ($this->update($data, ["id" => $user->id])) {
                $user = $this->read(false, ["*"], ["id" => $user->id]);
                return $user;
            } else {
                setFlashMessage("danger", ["Não foi possível atualizar seus dados, tente mais tarde. Erro: " . $this->error()]);
            }
        }

        return false;
    }

    private function removeFile($folder, $file)
    {
        if (!empty($file))
        {
            $filePath = $folder."/".$file;

            if (file_exists($filePath)) 
            {
                @unlink($filePath);
            }
        }
    }

    public function exists($field, $value, $id = null)
    {
        if ($id) {
            return $this->read(false, ["*"], [$field => $value], ["id", "NOT IN", [$id]]);
        }

        return $this->read(false, ["*"], [$field => $value]);
    }

    public function destroy($id)
    {
        return $this->delete(["id" => $id]);
    }

    private function buildWhere($filters = [])
    {
        $where = [];

        if (isset($filters["searchTerm"]) && !empty($filters["searchTerm"])) {
            $where[] = "(users.name LIKE '%" . $filters["searchTerm"] . "%' OR users.email LIKE '%" . $filters["searchTerm"] . "%') ";
        }

        if (isset($filters["users"]) && is_array($filters["users"])) {
            $where[] = "users.id IN ('".implode("','",$filters["users"])."')";
        }

        return $where;
    }

    public function getFollowings($user_id)
    {
        $followingIds = (new UserFollow())->getFollowing($user_id, false);

        $results = $this->all(["users" => $followingIds], 9);

        return $results;
    }

    public function getFollowers($user_id)
    {
        $followersId = (new UserFollow())->getFollower($user_id, false);

        $results = $this->all(["users" => $followersId]);

        return $results;
    }

    public function follow($user_id_from, $user_id_to)
    {
        $userFollow = new UserFollow();
        if (!$userFollow->checkFollow($user_id_from, $user_id_to)) {
            $userFollow->insert([
                "user_id_from" => $user_id_from,
                "user_id_to" => $user_id_to
            ]);
        } else {
            $userFollow->delete([
                "user_id_from" => $user_id_from,
                "user_id_to" => $user_id_to
            ]);
        }
        return true;
    }
}

?>