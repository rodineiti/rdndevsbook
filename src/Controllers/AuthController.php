<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\User;

class AuthController extends Controller
{
    protected $user;
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->user = new User();
        $this->data = array();
        $this->required = ["name", "email", "password", "birthdate"];
    }

    public function index()
    {
        $this->template("login", $this->data);
    }

    public function register()
    {
        $this->template("register", $this->data);
    }

    public function login()
    {
        if ($this->method() !== "POST") {
            setFlashMessage("danger", ["Método não permitido"]);
            $this->redirect("auth?login");
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if(isset($request["email"]) && !empty($request["email"])) {
            $email = $request["email"];
            $password = $request["password"];

            $user = $this->user->attempt($email, $password);

            if (!$user) {
                setFlashMessage("danger", ["Não foi encontrado nenhum usuário com os dados enviados"]);
                $this->redirect("auth?login");
            }

            $this->user->setSession($user);

            $this->redirect("home");
        }

        setFlashMessage("danger", ["Favor informar seu e-mail e sua senha de acesso"]);
        $this->redirect("auth?login");
    }

    public function save()
    {
        if ($this->method() !== "POST") {
            setFlashMessage("danger", ["Método não permitido"]);
            $this->redirect("auth/register");
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);

        if(!$this->required($request)) {
            setFlashMessage("danger", ["Favor preencher seu e-mail e sua senha"]);
            $this->redirect("auth/register");
        }

        $user = $this->user->create($request);

        if (!$user) {
            setFlashMessage("danger", ["Não foi possível criar sua conta, tente mais tarde."]);
            $this->redirect("auth/register");
        }

        setFlashMessage("success", ["Sua conta foi criada com sucesso, faça o login agora."]);
        $this->redirect("auth/register");
    }

    public function update()
    {
        if ($this->method() !== "POST") {
            setFlashMessage("danger", ["Método não permitido"]);
            $this->redirect("settings");
        }

        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $request["avatarFile"] = isset($_FILES['avatar']) && !empty($_FILES["avatar"]["tmp_name"]) ? $_FILES['avatar'] : [];
        $request["coverFile"] = isset($_FILES['cover']) && !empty($_FILES["cover"]["tmp_name"]) ? $_FILES['cover'] : [];;

        if($request) {
            $user = $this->user->updateProfile(auth(), $request);
            if (!$user) {
                setFlashMessage("danger", ["Não foi possível atualizar seus dados, tente mais tarde."]);
                $this->redirect("settings");
            } else {
                $this->user->setSession($user);

                setFlashMessage("success", ["Dados atualizados com sucesso."]);
                $this->redirect("settings");
            }
        }

        setFlashMessage("danger", ["Não foi possível atualizar seus dados, tente mais tarde."]);
        $this->redirect("settings");
    }

    public function logout()
    {
        session_start();
        $this->user->destroySession();
        $this->redirect();
    }
}