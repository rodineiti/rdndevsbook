function setActiveTab(tab) {
    document.querySelectorAll('.tab-item').forEach(function(e){
        if(e.getAttribute('data-for') == tab) {
            e.classList.add('active');
        } else {
            e.classList.remove('active');
        }
    });
}
function showTab() {
    if(document.querySelector('.tab-item.active')) {
        let activeTab = document.querySelector('.tab-item.active').getAttribute('data-for');
        document.querySelectorAll('.tab-body').forEach(function(e){
            if(e.getAttribute('data-item') == activeTab) {
                e.style.display = 'block';
            } else {
                e.style.display = 'none';
            }
        });
    }
}

if(document.querySelector('.tab-item')) {
    showTab();
    document.querySelectorAll('.tab-item').forEach(function(e){
        e.addEventListener('click', function(r) {
            setActiveTab( r.target.getAttribute('data-for') );
            showTab();
        });
    });
}

var feedNew = document.querySelector('.feed-new-input-placeholder');

if (feedNew) {
    feedNew.addEventListener('click', function(obj){
        obj.target.style.display = 'none';
        document.querySelector('.feed-new-input').style.display = 'block';
        document.querySelector('.feed-new-input').focus();
        document.querySelector('.feed-new-input').innerText = '';
    });

    document.querySelector('.feed-new-input').addEventListener('blur', function(obj) {
        let value = obj.target.innerText.trim();
        if(value === '') {
            obj.target.style.display = 'none';
            document.querySelector('.feed-new-input-placeholder').style.display = 'block';
        }
    });
}

if (document.querySelector(".notification")) {
    let notification = document.querySelector(".notification");
    setTimeout(function () {
        notification.style.display = "none";
    }, 10000);
}

if (document.querySelector(".like-btn")) {
    document.querySelectorAll(".like-btn").forEach(function (item) {
        item.addEventListener("click", function () {
            let id = item.closest(".feed-item").getAttribute("data-id");
            let count = parseInt(item.innerText);
            if (item.classList.contains("on") === false) {
                item.classList.add("on");
                item.innerText = ++count;
            } else {
                item.classList.remove("on");
                item.innerText = --count;
            }
            fetch(baseUrl + "ajax/like/" + id);
        });
    });
}

if (document.querySelector(".fic-item-field")) {
    document.querySelectorAll(".fic-item-field").forEach(function (item) {
        item.addEventListener("keyup", async function (e) {
            if (e.keyCode === 13) {
                let id = item.closest(".feed-item").getAttribute("data-id");
                let txt = item.value;
                item.value = "";

                let data = new FormData();
                data.append("id", id);
                data.append("body", txt);

                let request = await fetch(baseUrl + "ajax/comment", {
                    method: "POST",
                    body: data
                });
                let json = await request.json();

                if (!json.error) {
                    let html = '<div class="fic-item row m-height-10 m-width-20">\n' +
                        '    <div class="fic-item-photo">\n' +
                        '        <a href="'+baseUrl+json.data.url+'">\n' +
                        '            <img src="'+json.data.user.avatar+'" alt="avatar" />\n' +
                        '        </a>\n' +
                        '    </div>\n' +
                        '    <div class="fic-item-info">\n' +
                        '        <a href="'+baseUrl+json.data.url+'">'+json.data.user.name+'</a>\n' +
                        '        '+json.data.body+'\n' +
                        '    </div>\n' +
                        '</div>';

                    item.closest(".feed-item").querySelector(".fic-item-area").innerHTML += html;
                }
            }
        });
    });
}

function closeFeedWindom() {
    document.querySelectorAll(".feed-item-more-window").forEach(function (item) {
        item.style.display = "none";
    });
    document.removeEventListener("click", closeFeedWindom)
}

if (document.querySelector(".feed-item-head-btn")) {
    document.querySelectorAll(".feed-item-head-btn").forEach(function (item) {
        item.addEventListener("click", function () {
            closeFeedWindom();

            item.querySelector(".feed-item-more-window").style.display = "block";
            setTimeout(function () {
                document.addEventListener("click", closeFeedWindom)
            });
        });
    });
}