<?php

namespace Src\Models;

use Src\Core\Model;

class PostLike extends Model
{
    public function __construct()
    {
        parent::__construct("posts_likes");
    }

    public function getCountByPost($post_id)
    {
        return $this->count(["id"], ["post_id" => $post_id]);
    }

    public function getCountByPostAndUser($post_id, $user_id)
    {
        return $this->count(["id"], ["post_id" => $post_id, "user_id" => $user_id]);
    }

    public function like($post_id, $user_id)
    {
        if (!$this->getCountByPostAndUser($post_id, $user_id)) {
            $this->insert([
                "post_id" => $post_id,
                "user_id" => $user_id
            ]);
        } else {
            $this->delete([
                "post_id" => $post_id,
                "user_id" => $user_id
            ]);
        }
        return true;
    }
}

?>