<?php

namespace Src\Controllers;

use Src\Core\Controller;
use Src\Models\Post;
use Src\Models\UserFollow;

class HomeController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
    }

    public function index()
    {
        $request = filter_var_array($this->request(), FILTER_SANITIZE_STRIPPED);
        $limit = $this->limit;
        $page = !empty($request["page"]) ? intval($request["page"]) : 1;
        $offset = (($page * $limit) - $limit);

        $results = (new Post())->all([], ["posts.user_id", "IN", (new UserFollow())->getFollowing(auth()->id)], $limit, $offset);
        $resultsCount = (new Post())->count(["id"], [], ["posts.user_id", "IN", (new UserFollow())->getFollowing(auth()->id)]);
        $pages = ceil($resultsCount / $limit);

        $this->data["posts"] = $results;
        $this->data["total"] = count($results);
        $this->data["pages"] = $pages;
        $this->data["page"] = $page;
        $this->template("home", $this->data);
    }
}