# RDNDEVBOOKS

Clone the repository

    git clone git@gitlab.com:rodineiti/rdndevsbook.git rdndevsbook

Switch to the repo folder

    cd rdndevsbook
    cp config-example.php config.php
    cp htaccess-example.txt .htaccess
    
Edit file config.php, and set connection mysql

    $config["dbname"] = "rdndevsbook";
    $config["dbhost"] = "mysql";
    $config["dbuser"] = "root";
    $config["dbpass"] = "root";
    
Dump file rdndevsbook.sql into database

Run server php or your server (Wamp, Mamp, Xamp), and open in the browser localhost
  
    php -S localhost

![image](https://user-images.githubusercontent.com/25492122/90561918-caeb5680-e177-11ea-9f72-9183b7829d77.png)