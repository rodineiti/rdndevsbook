<aside class="mt-10">
    <nav>
        <a href="<?=BASE_URL?>">
            <div class="menu-item <?=setMenuActive(["home"])?>">
                <div class="menu-item-icon">
                    <img src="<?=image("home-run.png")?>" width="16" height="16" alt="home" />
                </div>
                <div class="menu-item-text">
                    Home
                </div>
            </div>
        </a>
        <a href="<?=BASE_URL?>profile">
            <div class="menu-item <?=setMenuActive(["profile"])?>">
                <div class="menu-item-icon">
                    <img src="<?=image("user.png")?>" width="16" height="16" alt="user" />
                </div>
                <div class="menu-item-text">
                    Meu Perfil
                </div>
            </div>
        </a>
        <a href="<?=BASE_URL?>friends">
            <div class="menu-item <?=setMenuActive(["friends"])?>">
                <div class="menu-item-icon">
                    <img src="<?=image("friends.png")?>" width="16" height="16" alt="friends" />
                </div>
                <div class="menu-item-text">
                    Amigos
                </div>
            </div>
        </a>
        <a href="<?=BASE_URL?>photos">
            <div class="menu-item <?=setMenuActive(["photos"])?>">
                <div class="menu-item-icon">
                    <img src="<?=image("photo.png")?>" width="16" height="16" alt="photo" />
                </div>
                <div class="menu-item-text">
                    Fotos
                </div>
            </div>
        </a>
        <div class="menu-splitter"></div>
        <a href="<?=BASE_URL?>settings">
            <div class="menu-item">
                <div class="menu-item-icon">
                    <img src="<?=image("settings.png")?>" width="16" height="16" alt="settings" />
                </div>
                <div class="menu-item-text">
                    Configurações
                </div>
            </div>
        </a>
        <a href="<?=BASE_URL?>auth/logout">
            <div class="menu-item">
                <div class="menu-item-icon">
                    <img src="<?=image("power.png")?>" width="16" height="16" alt="power" />
                </div>
                <div class="menu-item-text">
                    Sair
                </div>
            </div>
        </a>
    </nav>
</aside>