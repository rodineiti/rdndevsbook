<div class="head-side">
    <div class="head-side-left">
        <div class="search-area">
            <form method="GET" action="<?=BASE_URL?>search">
                <input type="search" autocomplete="off" placeholder="Pesquisar" name="term" />
            </form>
        </div>
    </div>
    <div class="head-side-right">
        <a href="<?=BASE_URL?>profile" class="user-area">
            <div class="user-area-text"><?=auth()->name?></div>
            <div class="user-area-icon">
                <?php if (auth()->avatar): ?>
                    <img src="<?=media("avatars/".auth()->avatar)?>" alt="avatar" />
                <?php else: ?>
                    <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                <?php endif; ?>
            </div>
        </a>
        <a href="<?=BASE_URL?>auth/logout" class="user-logout">
            <img src="<?=image("power_white.png")?>" alt="powerwhite" />
        </a>
    </div>
</div>