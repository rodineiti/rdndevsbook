<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=SITE_NAME?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php if (!auth()): ?>
        <link rel="stylesheet" href="<?=asset("css/login.css")?>">
    <?php else: ?>
        <link rel="stylesheet" href="<?=asset("css/style.css")?>">
    <?php endif; ?>
    <script src="<?=asset("js/vanillaModal.js")?>"></script>
</head>
<body>
<header>
    <div class="container">
        <?php if (!auth()): ?>
            <a href="<?=BASE_URL?>"><img src="<?=image("devsbook_logo.png")?>" alt="logo" /></a>
        <?php else: ?>
            <div class="logo">
                <a href="<?=BASE_URL?>"><img src="<?=image("devsbook_logo.png")?>" alt="logo" /></a>
            </div>
            <?=$this->view("header")?>
        <?php endif; ?>
    </div>
</header>
<section class="container main">
    <?php $this->viewTemplate($view, $data); ?>
</section>
<?php $this->view("modal"); ?>
<script>var baseUrl = '<?=BASE_URL?>';</script>
<script src="<?=asset("js/script.js")?>"></script>
</body>
</html>