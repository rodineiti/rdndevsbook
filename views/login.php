<?=$this->view("alerts")?>
<form method="POST" action="<?= BASE_URL?>auth/login">
    <input placeholder="Digite seu e-mail" class="input" type="email" name="email" />
    <input placeholder="Digite sua senha" class="input" type="password" name="password" />
    <input class="button" type="submit" value="Acessar o sistema" />
    <a href="<?= BASE_URL?>auth/register">Ainda não tem conta? Cadastre-se</a>
</form>