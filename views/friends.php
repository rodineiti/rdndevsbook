<?=$this->view("aside")?>

<section class="feed">

    <?=$this->view("profile_header", ["user" => $user])?>

    <div class="row">

        <div class="column">

            <div class="box">
                <div class="box-body">

                    <div class="tabs">
                        <div class="tab-item" data-for="followers">
                            Seguidores
                        </div>
                        <div class="tab-item active" data-for="following">
                            Seguindo
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-body" data-item="followers">

                            <div class="full-friend-list">
                                <?php foreach ($user->followers as $follower):?>
                                <div class="friend-icon">
                                    <a href="<?=BASE_URL . "profile/show/".$follower->id?>">
                                        <div class="friend-icon-avatar">
                                            <?php if ($follower->avatar): ?>
                                                <img src="<?=media("avatars/".$follower->avatar)?>" alt="avatar" />
                                            <?php else: ?>
                                                <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                                            <?php endif; ?>
                                        </div>
                                        <div class="friend-icon-name">
                                            <?=$follower->name?>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        <div class="tab-body" data-item="following">

                            <div class="full-friend-list">
                                <?php foreach ($user->followings as $following):?>
                                <div class="friend-icon">
                                    <a href="<?=BASE_URL . "profile/show/".$following->id?>">
                                        <div class="friend-icon-avatar">
                                            <?php if ($following->avatar): ?>
                                                <img src="<?=media("avatars/".$following->avatar)?>" alt="avatar" />
                                            <?php else: ?>
                                                <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                                            <?php endif; ?>
                                        </div>
                                        <div class="friend-icon-name">
                                            <?=$following->name?>
                                        </div>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

</section>