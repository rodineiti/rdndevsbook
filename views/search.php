<?=$this->view("aside")?>

<section class="feed">

    <div class="row mt-10">
        <div class="column pr-5">

            <h1>Você pesquisou por: <?=$term?></h1>

            <div class="full-friend-list">
                <?php foreach ($users as $user):?>
                    <div class="friend-icon">
                        <a href="<?=BASE_URL . "profile/show/".$user->id?>">
                            <div class="friend-icon-avatar">
                                <?php if ($user->avatar): ?>
                                    <img src="<?=media("avatars/".$user->avatar)?>" alt="avatar" />
                                <?php else: ?>
                                    <img src="<?=media("avatars/avatar.jpg")?>" alt="avatar" />
                                <?php endif; ?>
                            </div>
                            <div class="friend-icon-name">
                                <?=$user->name?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>

        <?=$this->view("sponsor")?>
    </div>

</section>