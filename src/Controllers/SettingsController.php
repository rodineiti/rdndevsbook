<?php

namespace Src\Controllers;

use Src\Core\Controller;

class SettingsController extends Controller
{
    protected $data;
    protected $required;

    public function __construct()
    {
        parent::__construct();
        $this->auth();
        $this->data = array();
    }

    public function index()
    {
        $this->template("settings", $this->data);
    }
}